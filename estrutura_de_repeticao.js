

//comentario de 1 linha
/*
bloco de comentario, varias linhas
*/

let numero = 0;
while(numero <= 10){ // WHILE processa primeiro, e executa no final
    
    if(numero%2 == 0){//contador que divide por 2, mas o resultado é o resto
                    //== significa comparação
        console.log(`valor nr: ${(numero)} é PAR!`);
    }else{
        console.log(`valor nr: ${(numero)}`);
    }
    numero++; //contador, também é o "i"
}




var numero1 = 0;
console.log("REPETIÇÃO DO/WHILE");
do{   
    
    if(numero1%2 == 0){
    console.log(`valor nr: ${(numero1)} é PAR!`);
    }else{
    console.log(`valor nr: ${(numero1)}`);
    }
    numero1++;
}while(numero1 <= 10); //Do While executa no começo, ao menos 1 vez, para depois processar




console.log("REPETIÇÃO FOR");
for(let numero2 = 0; numero2 <= 10; numero2++){
    
    if(numero2%2 == 0){
    console.log(`valor nr: ${(numero2)} é PAR!`);
    }else{
    console.log(`valor nr: ${(numero2)}`);
    }
}